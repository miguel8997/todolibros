package cl.dci.ufro.TodoLibros;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TodoLibrosApplication {

	public static void main(String[] args) {
		SpringApplication.run(TodoLibrosApplication.class, args);
	}

}
