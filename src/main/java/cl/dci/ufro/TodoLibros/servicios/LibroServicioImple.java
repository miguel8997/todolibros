package cl.dci.ufro.TodoLibros.servicios;

import cl.dci.ufro.TodoLibros.modelo.Libro;
import cl.dci.ufro.TodoLibros.repositorios.LibroRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LibroServicioImple implements LibroServicio {

    @Autowired
    private LibroRepositorio libroRepositorio;

    @Override
    public List<Libro> mostrarLibros() {
        return libroRepositorio.findAll();
    }

    @Override
    public void guardarLibro(Libro libro) {
        libroRepositorio.save(libro);
    }

    @Override
    public void eliminarCatalogo() {
        libroRepositorio.deleteAll();
    }
}
