package cl.dci.ufro.TodoLibros.repositorios;

import cl.dci.ufro.TodoLibros.modelo.Libro;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LibroRepositorio extends JpaRepository<Libro,Integer> {
}
