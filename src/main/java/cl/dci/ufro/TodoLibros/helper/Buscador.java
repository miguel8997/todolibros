package cl.dci.ufro.TodoLibros.helper;

import cl.dci.ufro.TodoLibros.modelo.Libro;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Buscador {
    private List<Libro> cadenas;
    private List<String> palabras;
    private List<Libro> encontradas;
    private int nencontradas = 0;

    public Buscador() {
        cadenas= new ArrayList<Libro>();
        palabras = new ArrayList<String>();
        encontradas = new ArrayList<Libro>();
    }

    public List<Libro> palabrasClaves(List<Libro> libros, String palabraBuscar){

        this.cadenas=libros;

        palabras.add(palabraBuscar);
        if (busqueda()) {
            for (Libro encontrada: encontradas) {
                System.out.println(encontrada.getNombre());
            }
            return encontradas;
        } else {
            System.out.println("NO SE ENCONTRARON COINCIDENCIAS EN LA BUSQUEDA");
            return null;
        }
    }


    public boolean busqueda() {
        String patron = String.join("|", palabras);
        for (Libro cadena: cadenas) {
            Pattern pattern = Pattern.compile("\\b(" + patron + ")\\b", Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(cadena.getNombre());
            String cadena_modif = "";
            while (matcher.find()) {
                nencontradas++;
                cadena_modif = cadena.getNombre().replaceAll("\\b(?i)(" + matcher.group(1) + ")\\b", "<<$1>>");
            }
            if( ! cadena_modif.isEmpty()) encontradas.add(cadena);
        }
        if (encontradas.size() > 0) return true;
        return false;
    }
}
