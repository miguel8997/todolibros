package cl.dci.ufro.TodoLibros.helper;

import cl.dci.ufro.TodoLibros.modelo.Libro;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AticoLibros {
    private String nombre;
    private String url;
    private Document doc;

    public AticoLibros(String url,String nombre) {
        this.url = url;
        this.nombre=nombre;
    }

    public List<Libro> loadContentPage() {
        List<Libro> librosEncontrados=new ArrayList<>();
        try {
            for (int i = 128; i <= 128; i++) {
                this.doc = Jsoup.connect(this.url+"2-inicio?page="+i).get();
                sacarDatos(this.doc,librosEncontrados);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return librosEncontrados;
    }

    public void sacarDatos(Document d,List<Libro> librosEncontrados){
        Elements tables = d.select("article.product-miniature");
        for(Element list: tables){
            String url= list.select("a").attr("href");
            String imgSrc= list.select("img").attr("src");
            Libro libro=datosLibro(url);
            libro.setUrlCompra(url);
            libro.setUrlImg(imgSrc);
            librosEncontrados.add(libro);

        }
    }

    public Libro datosLibro(String url){

        Libro l=new Libro();
        try {
            Document doc = Jsoup.connect(url).get();
            Elements temp = doc.select("section#main");

            String nombre=temp.select("div.col-md-7 h1").text();
            l.setNombre(nombre);

            String precioOferta=temp.select("div.current-price span").first().attr("content");
            l.setPrecioAhora(Integer.parseInt(precioOferta));

            String precioNormal=temp.select("div.product-discount span").text();
            precioNormal=precioNormal.replace("$ ","");
            precioNormal=precioNormal.replace(".","");
            l.setPrecioAntes(Integer.parseInt(precioNormal));

            String porcientoDescuento=temp.select("div.current-price span.discount").text();
            porcientoDescuento=porcientoDescuento.replace("% de descuento","");
            l.setPorcientoDescuento(Integer.parseInt(porcientoDescuento));

//            String stock=temp.select("div.product-quantities span").attr("data-stock");
//            l.setStock(Integer.parseInt(stock));

            String editorial=temp.select("div.product-manufacturer span").text();
            l.setEditorial(editorial);

//            String autor=temp.select("section.product-features dd.value").first().text();
//            l.setAutor(autor);

//            String anio=temp.select("div#metadata-ano.box").text();
//            String idioma=temp.select("div#metadata-idioma.box").text();
//            String descripcion=temp.select("div.product-information p").first().text();
//            String categoria=temp2.select("li a span").first().next().text();
            System.out.println("1");

        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }

        return l;
    }

}
