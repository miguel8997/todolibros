package cl.dci.ufro.TodoLibros.helper;

import cl.dci.ufro.TodoLibros.modelo.Libro;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BuscaLibre {
    private String nombre;
    private String url;
    private Document doc;

    public BuscaLibre(String url,String nombre) {
        this.url = url;
        this.nombre=nombre;
    }

    public List<Libro>  loadContentPage() {
        List<Libro> librosEncontrados=new ArrayList<>();
        try {
            for (int i = 128; i <= 128; i++) {
                this.doc = Jsoup.connect(this.url+"libros-envio-express-chile_t.html?page="+i).get();
                if (this.doc.select("section#noEncontrado.noExiste p").text().
                        equalsIgnoreCase("Lo sentimos, pero no encontramos lo que buscas: La busqueda debe tener al menos 3 caracteres")){
                    break;
                }
                sacarDatos(this.doc,librosEncontrados);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return librosEncontrados;
    }

    public void sacarDatos(Document d, List<Libro> librosEncontrados){
        Elements tables = d.select("div.producto");
        for(Element list: tables){
            String url= list.select("a").attr("href");
            String imgSrc = list.select("img").attr("data-src");
            Libro libro=datosLibro(url);
            libro.setUrlCompra(url);
            libro.setUrlImg(imgSrc);
            librosEncontrados.add(libro);

        }
    }

    public Libro datosLibro(String url){
        Libro l=new Libro();
        try {
            Document doc = Jsoup.connect(url).get();
            Elements temp = doc.select("div.row.product-info");

            String nombre=temp.select("div.row.product-info h1").text();
            l.setNombre(nombre);

            String precioAhora=temp.select("p.precioAhora span").first().text();
//            precioAhora=precioAhora.replace("$ ","");
//            precioAhora=precioAhora.replace(".","");
//            l.setPrecioOferta(Integer.parseInt(precioAhora));

            String precioAntes=temp.select("p.precioAntes").first().text();
//            precioAntes=precioAntes.replace("$ ","");
//            precioAntes=precioAntes.replace(".","");
//            l.setPrecioNormal(Integer.parseInt(precioAntes));

            String porcientoDescuento="0";
//            porcientoDescuento=porcientoDescuento.replace("%","");
//            l.setPorcientoDescuento(Integer.parseInt(porcientoDescuento));

            if (precioAntes.length()==0){
                precioAhora=precioAhora.replace("$ ","");
                precioAhora=precioAhora.replace(".","");
                l.setPrecioAhora(Integer.parseInt(precioAhora));
                l.setPrecioAntes(0);
                l.setPorcientoDescuento(0);
            }else{
                porcientoDescuento=temp.select("div.box-descuento.font-weight-light strong").first().text();
                precioAhora=precioAhora.replace("$ ","");
                precioAhora=precioAhora.replace(".","");
                l.setPrecioAhora(Integer.parseInt(precioAhora));
                precioAntes=precioAntes.replace("$ ","");
                precioAntes=precioAntes.replace(".","");
                l.setPrecioAntes(Integer.parseInt(precioAntes));
                porcientoDescuento=porcientoDescuento.replace("%","");
                l.setPorcientoDescuento(Integer.parseInt(porcientoDescuento));
            }

            String stock=temp.select("div.box-comprar ul li.font-size-small.color-green.margin-right-10.margin-bottom-10").first().text();
            if (stock.charAt(5)=='n'){
                stock=stock.replace("Quedan ","");
                stock=stock.replace(" unidades","");
                l.setStock(Integer.parseInt(stock));
            }else{
                stock=stock.replace("Queda ","");
                stock=stock.replace(" unidad","");
                l.setStock(Integer.parseInt(stock));
            }

            String autor=temp.select("div#metadata-autor.box a").text();
            l.setAutor(autor);

            String editorial=temp.select("div#metadata-editorial.box a").text();
            l.setEditorial(editorial);
//            String anio=temp.select("div#metadata-ano.box").text();
//            String idioma=temp.select("div#metadata-idioma.box").text();
//            String descripcion=temp.select("div.descripcionBreve p").text();
//            String categoria=temp.select("div#metadata-categoría.box").text();
            System.out.println("2");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return l;
    }
}
